#!/usr/bin/bash	

sys_info="@@null@@" # clear sys_info variable
if [ -f "/etc/issue" ];
then
	if [[ `cat /etc/issue` == *"Arch Linux"* ]];
	then
		sys_info="arch" # set sys_info to "arch" if /etc/issue exists and contains "Arch Linux"
	fi
fi

if [[ `pwd` == *"com.termux"* ]];
then
	sys_info="termux" # set sys_info to "termux" if the path to the current working directory contains "com.termux"
fi

# install dependencies
case $sys_info in
	"arch")
		if [ $EUID == 0 ];
		then
			echo ERROR!: This script must be run as user. Doing nothing and quitting.
			exit # if Arch Linux, but root, exit
		fi

		command -v "sudo" >/dev/null && echo Installing system dependencies using pacman... || { echo "ERROR!: This script requires that \`sudo\` is installed and configured for the target user when on Arch Linux. Doing nothing and quitting."; exit 1; }
		sudo pacman -Sy --needed python python-pip exa neovim nodejs git base-devel neofetch # update local repositories and install dependencies
		echo Finished installing system dependencies using pacman!
		;;
	"termux")
		echo Installing system dependencies using pkg...
		pkg update # update local repositories and upgrade packages
		pkg install python exa neovim nodejs git build-essential neofetch # install dependencies
		echo Finished installing system dependencies using pkg...
		;;
	*)
		echo ERROR!: Unexpected environment! This script is intented to be run on Termux and Arch Linux. Running it within other environments may cause fatal breakage and is not supported.
		exit # if unexpected environment, exit
		;;
esac

# install Python dependencies
echo
echo Installing Python dependencies using pip3...
pip3 install pynvim neovim-remote --upgrade
echo Finished installing Python dependencies using pip3!

# copy neofetch art
neofetch_art_orig="$HOME/.neofetch_art"
neofetch_art_new="`pwd`/.neofetch_art"
cp $neofetch_art_new $neofetch_art_orig
echo
echo \"$neofetch_art_new\" has been copied to \"$neofetch_art_orig\"!

# install 'nickgirga/nvim-edit'
echo
echo Installing \'nickgirga/nvim-edit\'
cd ./nvim-edit
bash ./install.sh
cd ..
echo Finished installing \'nickgirga/nvim-edit\'

# configure neovim
echo
echo Configuring Neovim...
cd ./nvim-config
bash ./install.sh
cd ..
echo Finished Configuring Neovim!

# inform user of completion
echo
echo Done!
echo
