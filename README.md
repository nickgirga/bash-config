# bash-config
This is a simple installer script that makes it easy to deploy my bash configurations (both user and root) on Termux (for my Android) and Arch Linux (for my desktop). It creates numerous aliases that expect certain programs with certain configurations. When everything is used together, it can create a comfortable development environment.

# Obtaining
To download everything, use `git clone --recurse-submodles https://gitlab.com/nickgirga/bash-config.git`. Note that `--recurse-submodules` is required because the dependency installation script uses 2 submodules that must be fetched before use. Now that you have all the files, head to the [Installation](#installation) section.

# Installation
First, [obtain](#obtaining) the configuration files and install scripts. Navigate to the root directory of the repository. If you just cloned it, you can do this with `cd bash-config`. Then, run `./install-deps.sh` or `bash ./install-deps.sh` to install the dependencies that you will need to use the configuration efficiently. Once it finishes, run `./install.sh` or `bash ./install.sh`. After that completes, you must restart your terminal for the changes to take place. Once the terminal is restarted, you might want to run `nvim +PlugInstall` just once to download and set up the new plugins for Neovim. After the plugins complete installing, you can quit Neovim by pressing `Escape` to enter command mode and typing `:q` to quit. Because we launched Neovim with the plugin installer, it will bring us back to a blank document in Neovim, so we must use `:q` again to quit entirely.

# Single-Command Installation (Not Recommended)
WARNING!: This does a lot of really scary stuff really fast. You may want to walk through the steps manually to make sure this configuration works on your device before forcing it to do everything at once.

This single command should (if compatible with your system) obtain the needed files, install dependencies, and install the bash configuration. Paste, press enter, and confirm a few prompts to get up and running with the exact same setup everywhere. Make sure you read every prompt carefully.

`git clone --recurse-submodules https://gitlab.com/nickgirga/bash-config.git && cd bash-config/ && bash ./install-deps.sh && bash ./install.sh`

If anything bad happens during the install, do *NOT* run the single-command installation again! First, delete the `bash-config` directory that git created. Then, follow the manual installation steps by boing to the [Obtaining](#obtaining) section.
