#!/usr/bin/bash
echo
echo \'nickgirga/bash-config\' Installer
echo Copyright \(c\) 2022 Nicholas Girga
echo
echo WARNING!: This script will replace user and root bash configuration files. 
echo The original files will be backed up as hidden files in their original configuration directory. 
echo Read warnings very carefully. Breaking bash may even make accessing these backup files difficult. 
echo
echo This installer is released under an MIT license and comes with ABSOLUTELY NO WARRANTY.
echo By entering \'y\' or \'Y\' you are agreeing to take full responsibility for any damage done to your device.
echo
read -r -p "Are you sure you wish to continue? [y/N] > " response
lowercase_response=`echo $response | tr '[:upper:]' '[:lower:]'`
if [[ $lowercase_response != "y" ]];
then
	echo
	echo Quitting \(user request\)... # quit unless user responds with y/Y
	echo
 	exit
fi

# Start of backing up configuration
echo
echo Backing up original configuration files...

file_date_and_time=`date +"%Y-%m-%d_%H-%M-%S-%N"` # get the current date and time

backup_ext="bash-config.${file_date_and_time}.backup" # the extension to append to backed up files

ubashrc_orig="$HOME/.bashrc"
if [ -f $ubashrc_orig ];
then
	ubashrc_backup="$ubashrc_orig.$backup_ext"
	cp "$ubashrc_orig" "$ubashrc_backup" # back up the user bashrc file
	echo \"$ubashrc_orig\" has been backed up to \"$ubashrc_backup\"!
else
	echo No configuration file found at path \($ubashrc_orig\). No backup needed. If you are using Termux and haven\'t created a bash configuration yet, this is normal. 
fi

rbashrc_orig="/etc/bash.bashrc"
if [ -f $rbash_orig ];
then
	rbashrc_backup="/etc/.bash.bashrc.$backup_ext"
	sudo cp "$rbashrc_orig" "$rbashrc_backup" # back up the root bashrc file
	echo \"$rbashrc_orig\" has been backed up to \"$rbashrc_backup\"!
else
	echo No configuration file found at path \($rbashrc_orig\). No backup needed. If you are using Termux, this is normal.
fi

echo Finished backing up original configuration files!
# End of backing up configuration


# Wait for use input
echo
echo WARNING!: The script is about to start the installation process, replacing the original configuration files. 
echo Ensure the original configuration files have been backed up successfully, without error before proceeding.
echo
read -r -p "Are you sure you wish to continue? [y/N] > " response
lowercase_response=`echo $response | tr '[:upper:]' '[:lower:]'`
if [[ $lowercase_response != "y" ]];
then
	echo
	echo Quitting \(user request\)... # quit unless user responds with y/Y
	echo
 	exit
fi


# Start of installation of 'nickgirga/bash-config' configuration
echo
echo Installing \'nickgirga/bash-config\' configuration files...

new_config_dir="`pwd`/config" # where the 'nickgirga/bash-config' configuration files are stored

ubashrc_new="@@null@@" # mark ubashrc_new as existing, but "null"

if [ -f "/etc/issue" ];
then
	if [[ `cat /etc/issue` == *"Arch Linux"* ]];
	then
	ubashrc_new="$new_config_dir/arch.bashrc" # use Arch Linux configuration if Arch Linux is in use
	fi
fi

if [[ `pwd` == *"com.termux"* ]];
then
	ubashrc_new="$new_config_dir/termux.bashrc" # use Termux configuration if Termux is in use
fi

if [ $ubashrc_new == "@@null@@" ]; # if ubashrc_new remains "null" (unexpected environment), ask user how to proceed
then
	echo WARNING!: Unexpected environment! This script is intended to be run on Termux \(on Android\) and Arch Linux \(on desktop\).
	echo It is not recommended that you continue on with the script. It may get you locked out of your device.
	echo
	echo Choose an option:
	echo [1] Do nothing and quit. \(Default and recommended\)
	echo [2] Install Arch Linux configuration. Requires root. \(NOT RECOMMENDED\)
	echo [3] Install Termux configuration. \(NOT RECOMMENDED\)
	echo
	read -r -p "How do you wish to proceed? > " response
	case $response in
		"2")
			echo
			echo WARNING!: Proceeding with Arch Linux configuration installation on an unexpected environment! This may get you locked out of your device!
			echo
			ubashrc_new="$new_config_dir/arch.bashrc" # use Arch Linux configuration
			;;
		"3")
			echo
			echo WARNING!: Proceeding with Termux configuration installation on an unexpected environment! This may get you locked out of your device!
			echo
			ubashrc_new="$new_config_dir/termux.bashrc" # use Termux configuration
			echo
			;;
		*)
			echo
			echo Doing nothing and quitting \(user request\)... # do nothing and quit
			echo
			exit
			;;
	esac
fi

cp $ubashrc_new $ubashrc_orig # install the 'nickgirga/bash-config' user configuration file
echo \"$ubashrc_new\" has been copied to \"$ubashrc_orig\"!

if [[ $ubashrc_new == *"arch.bashrc"* ]];
then
	rbashrc_new="$new_config_dir/etc/arch.bashrc"
	sudo cp $rbashrc_new $rbashrc_orig # install the 'nickgirga/bash-config' root configuration file if needed
	echo \"$rbashrc_new\" has been copied to \"$rbashrc_orig\"!
fi

echo Finished installing \'nickgirga/bash-config\' configuration files!
echo
echo Done!
echo
