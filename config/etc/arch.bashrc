#
# /etc/bash.bashrc
#

# 'nickgirga/bash-config'
# Arch Linux Root Configuration

# [Arch Linux Generated] If not running interactively, don't do anything
[[ $- != *i* ]] && return

# [Arch Linux Generated]
[[ $DISPLAY ]] && shopt -s checkwinsize

# [Arch Linux Generated]
PS1='[\u@\h \W]\$ '

# [Arch Linux Generated]
case ${TERM} in
  xterm*|rxvt*|Eterm|aterm|kterm|gnome*)
    PROMPT_COMMAND=${PROMPT_COMMAND:+$PROMPT_COMMAND; }'printf "\033]0;%s@%s:%s\007" "${USER}" "${HOSTNAME%%.*}" "${PWD/#$HOME/\~}"'

    ;;
  screen*)
    PROMPT_COMMAND=${PROMPT_COMMAND:+$PROMPT_COMMAND; }'printf "\033_%s@%s:%s\033\\" "${USER}" "${HOSTNAME%%.*}" "${PWD/#$HOME/\~}"'
    ;;
esac

# [Arch Linux Generated]
[ -r /usr/share/bash-completion/bash_completion   ] && . /usr/share/bash-completion/bash_completion

# Neovim Aliases
alias vibash='if [ "$EUID" == "0" ]; then nvim /etc/bash.bashrc; else nvim $HOME/.bashrc; fi'
alias nvim='NVIM_LISTEN_ADDRESS=/tmp/nvimsocket nvim'
alias vim='NVIM_LISTEN_ADDRESS=/tmp/nvimsocket nvim'
alias vi='NVIM_LISTEN_ADDRESS=/tmp/nvimsocket nvim'
alias neovim='NVIM_LISTEN_ADDRESS=/tmp/nvimsocket nvim'

# Neovim-like Exit Alias
alias ':q'='exit'

# Cool Retro Term Aliases
alias crt='cool-retro-term'
alias crts='screen cool-retro-term'

# Exa Aliases
alias ls='exa -al --color=always --group-directories-first'
alias la='exa -a --color=always --group-directories-first'
alias ll='exa -l --color=always --group-directories-first'
alias lt='exa -T --color=always --group-directories-first'
alias lt.='exa -aT --color=always --group-directories-first'
alias l.='exa -a | egrep "^\."'

# Grep Aliases
alias grep='grep --color=auto'
alias egrep='egrep --color=auto'
alias fgrep='fgrep --color=auto'

# Copy and Storage Space Aliases
alias cp="cp -i"
alias df='df -h'

# Extract Icon Alias
alias extract_icon="wrestool -x -t 14"

# Steam Environment Variables
STEAM_RUNTIME_PREFER_HOST_LIBRARIES=0
STEAM_RUNTIME=0
