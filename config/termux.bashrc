# 'nickgirga/bashrc'
# Termux User Configuration

# Arch Linux Alias (installed via AnLinux)
alias arch='current_dir=`pwd` && cd $HOME && ./start-arch.sh && cd $current_dir'

# Exa Aliases
alias ls='exa -al --color=always --group-directories-first'
alias la='exa -a --color=always --group-directories-first'
alias ll='exa -l --color=always --group-directories-first'
alias lt='exa -T --color=always --group-directories-first'
alias lt.='exa -aT --color=always --group-directories-first'

# Grep Aliases
alias grep='grep --color=auto'
alias egrep='egrep --color=auto'
alias fgrep='fgrep --color=auto'

# Copy and Storage Space Aliases
alias cp='cp -i'
alias df='df -h'

# Quick Config Edit Aliases
alias vibash='nvim $HOME/.bashrc'
alias viconf='nvim $HOME/.config/nvim/init.vim'

# Neovim Aliases
alias neovim='nvim'
alias vim='nvim'
alias vi='nvim'
alias ':edit'='$HOME/.local/bin/nvim-edit'

# Neovim-like Exit Alias
alias ':q'=exit

# Display System Information 
clear
neofetch --color_blocks off

# Arch Linux Instructions
echo Type \`arch\` and press enter to boot into Arch Linux.
echo Type \`help\` and press enter for more option.
echo
