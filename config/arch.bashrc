#
# ~/.bashrc
#

# 'nickgirga/bash-config'
# Arch Linux User Configuration

# [Arch Linux Generated] If not running interactively, don't do anything
[[ $- != *i* ]] && return

# [Arch Linux Generated]
PS1='[\u@\h \W]\$ '

# Neovim Aliases
alias viconf='nvim $HOME/.config/nvim/init.vim'
alias ':edit'='$HOME/.local/bin/nvim-edit'
alias ':gdserver'='nvim --listen godothost +"VimFilerExplorer -project ."'

# Customize Neofetch
alias neofetch='neofetch --source "$HOME/.neofetch_art" --colors 58 --ascii_colors 58 --color_blocks off'

# Steam Environment Variables
STEAM_RUNTIME_PREFER_HOST_LIBRARIES=0
STEAM_RUNTIME=0

# Display System Information
echo
neofetch
echo
